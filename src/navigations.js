import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";
import MainScreen from "./containers/MainScreen";
const routeConfiguration = {
  Home: { screen: MainScreen }
};

const stackNavigatorConfiguration = {
  initialRouteName: "Home"
};

stackNavigatorConfiguration.navigationOptions = ({ navigation }) => {
  return {
    header: null
  };
};

export const Routes = createStackNavigator(routeConfiguration, stackNavigatorConfiguration);