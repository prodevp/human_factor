import React, { Component } from "react";
import { View, Button, Text } from "native-base";
import { StyleSheet, Dimensions, Image} from "react-native";

const { height, width} = Dimensions.get('window');

class MainScreen extends Component {

  render() {
    const {
      imageContainer,
      image,
      centered,
    } = styles;
    const { appOpenedTimes } = this.props;
    return (
      <View style={{ backgroundColor: "#6380ed",height,width }}>
       <View style={[centered, imageContainer]}>
          <Image source={{uri:'https://cdn2.iconfinder.com/data/icons/mind-process-1/64/KEY_SUCCESS_FACTORS-512.png'}} style={image} resizeMode="contain" />
          <View style={{width,paddingHorizontal:15}}>
            <Button block style={{backgroundColor:'#fff'}}>
              <Text style={{color:'#6380ed'}}> Entrar </Text>
            </Button>
            <Button bordered light block style={{marginTop:10}}>
              <Text style={{color:'#fff'}}> Criar conta </Text>
            </Button>
          </View>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  centered: {
    flexDirection: "column",
    alignItems: "center"
  },
  imageContainer: {
    flex: 1,
    marginTop: -30
  },
  image: {
    width: 100,
    height : 500,
  },
});

export default MainScreen;
